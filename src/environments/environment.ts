// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAQq1bb3VVD6EBfhLtART6F5QwaAiKacOw",
    authDomain: "sms-verification-f21db.firebaseapp.com",
    databaseURL: "https://sms-verification-f21db.firebaseio.com",
    projectId: "sms-verification-f21db",
    storageBucket: "sms-verification-f21db.appspot.com",
    messagingSenderId: "230462005747",
    appId: "1:230462005747:web:296b31d7a493337bf97286"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
