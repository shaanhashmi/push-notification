import { Component, ChangeDetectorRef } from "@angular/core";
import { MessagingService } from "./services/messaging.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "push-notification";
  message;
  constructor(
    private messagingService: MessagingService,
    private cdRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.messagingService.requestPermission();
    this.messagingService.receiveMessage();
    this.messagingService.currentMessage.subscribe(res => {
      this.message = res;
      this.cdRef.detectChanges();
    });
  }
}
